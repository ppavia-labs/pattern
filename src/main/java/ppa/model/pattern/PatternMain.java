package ppa.model.pattern;

import ppa.model.pattern.creational.abstractFactory.AbstractClass;
import ppa.model.pattern.creational.abstractFactory.AbstractFactory;
import ppa.model.pattern.creational.abstractFactory.ConcreteFactoryA;
import ppa.model.pattern.creational.abstractFactory.ConcreteFactoryB;
import ppa.model.pattern.creational.builder.Builder;
import ppa.model.pattern.creational.builder.ComplexeObject;
import ppa.model.pattern.creational.builder.ConcreteBuilderA;
import ppa.model.pattern.creational.builder.ConcreteBuilderB;
import ppa.model.pattern.creational.builder.DirectorBuilder;

public class PatternMain {
	public static void main (String... args) {
		performAbstractFactoryPattern();
		performBuilderPattern();
	}
	
	/**
	 * Permet d'isoler l'appartenance à une famille de classes
	 */
	public static void performAbstractFactoryPattern() {
		AbstractFactory aFactory1	= new ConcreteFactoryA();
		AbstractFactory aFactory2	= new ConcreteFactoryB();
		
		AbstractClass aClass1		= aFactory1.createClass();
		AbstractClass aClass2		= aFactory2.createClass();
		
		aClass1.displayClass();
		aClass2.displayClass();
	}
	
	/**
	 * Permet d'isoler les variations de représentations d'un objet
	 */
	public static void performBuilderPattern() {
		Builder builderA	= new ConcreteBuilderA();
		Builder builderB	= new ConcreteBuilderB();
		DirectorBuilder dBuilderA	= new DirectorBuilder(builderA);
		DirectorBuilder dBuilderB	= new DirectorBuilder(builderB);
		
		ComplexeObject productA		= dBuilderA.createObject();
		ComplexeObject productB		= dBuilderB.createObject();
		
		productA.afficher();
		productB.afficher();
	}
	
	/**
	 * 
	 */
	public static void performFactoryMethodPattern () {
		
	}
}
