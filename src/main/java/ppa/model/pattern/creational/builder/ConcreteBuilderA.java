package ppa.model.pattern.creational.builder;

public class ConcreteBuilderA extends Builder {
	@Override
	public void createAttribute1(String pAttribute1) {
        product.setAttribut1 (pAttribute1 + " (avec dimension en centimètre) ");
	}

	@Override
	public void createAttribute2(Double pAttribute2) {
		product.setAttribut2(new Double(pAttribute2));		
	}
}
