package ppa.model.pattern.creational.builder;

public class DirectorBuilder {
	private Builder builder;
	
	public DirectorBuilder(Builder pBuilder) {
		builder	= pBuilder;
	}
	
	public ComplexeObject createObject() {
		builder.createObject();
		builder.createAttribute1("libelle de l'objet");
		builder.createAttribute2(12.0);
		return builder.getObject();
	}
}
