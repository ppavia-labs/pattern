package ppa.model.pattern.creational.builder;

public abstract class Builder {
	protected ComplexeObject product;
	
	public void createObject() {
		product	= new ComplexeObject();
	}
	
	public ComplexeObject getObject () {
		return product;
	}
	
	public abstract void createAttribute1 (String pAttribute1);
	public abstract void createAttribute2 (Double pAttribute2);
}
