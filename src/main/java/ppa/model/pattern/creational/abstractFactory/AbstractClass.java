package ppa.model.pattern.creational.abstractFactory;

public abstract class AbstractClass {
	public abstract void displayClass();
}
