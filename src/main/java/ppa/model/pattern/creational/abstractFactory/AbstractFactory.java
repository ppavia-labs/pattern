package ppa.model.pattern.creational.abstractFactory;

public interface AbstractFactory {
	public AbstractClass createClass ();
}
