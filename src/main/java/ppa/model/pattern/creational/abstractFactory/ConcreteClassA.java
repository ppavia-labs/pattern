package ppa.model.pattern.creational.abstractFactory;

public class ConcreteClassA extends AbstractClass {

	@Override
	public void displayClass() {
		System.out.println("class : " + this.getClass().getName());
	}

}
