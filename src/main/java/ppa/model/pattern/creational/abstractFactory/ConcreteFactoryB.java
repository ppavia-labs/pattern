package ppa.model.pattern.creational.abstractFactory;

public class ConcreteFactoryB implements AbstractFactory {

	@Override
	public AbstractClass createClass() {
		return new ConcreteClassB();
	}
}
