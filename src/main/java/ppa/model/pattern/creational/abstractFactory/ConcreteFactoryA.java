package ppa.model.pattern.creational.abstractFactory;

public class ConcreteFactoryA implements AbstractFactory {

	@Override
	public AbstractClass createClass() {
		return new ConcreteClassA();
	}
}
