package ppa.model.pattern.creational.abstractFactory;

public class ConcreteClassB extends AbstractClass {

	@Override
	public void displayClass() {
		System.out.println("class : " + this.getClass().getName());
	}

}
