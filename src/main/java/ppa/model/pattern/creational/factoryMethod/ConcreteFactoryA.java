package ppa.model.pattern.creational.factoryMethod;

public class ConcreteFactoryA extends AbstractFactory {

	@Override
	public AbstractClass createClass(boolean isClasseA) {
		if ( isClasseA ) {
			return new ConcreteClassA();
		}
		else {
			return new ConcreteClassB();
		}
	}

}
