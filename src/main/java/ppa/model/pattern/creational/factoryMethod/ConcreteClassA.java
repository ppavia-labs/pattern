package ppa.model.pattern.creational.factoryMethod;

public class ConcreteClassA implements AbstractClass {

	@Override
	public void displayClass() {
		System.out.println("class : " + this.getClass().getName());
	}

}
