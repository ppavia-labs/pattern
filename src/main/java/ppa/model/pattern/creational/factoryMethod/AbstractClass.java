package ppa.model.pattern.creational.factoryMethod;

public interface AbstractClass {
	public void displayClass();
}
