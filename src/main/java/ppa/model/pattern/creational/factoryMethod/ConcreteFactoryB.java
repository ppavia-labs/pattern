package ppa.model.pattern.creational.factoryMethod;

public class ConcreteFactoryB extends AbstractFactory {

	@Override
	public AbstractClass createClass(boolean isClasseA) {
		return new ConcreteClassB();
	}

}
