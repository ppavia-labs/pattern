package ppa.model.pattern.creational.factoryMethod;

public abstract class AbstractFactory {
	private boolean isClassA	= false;
	
	public abstract AbstractClass createClass (boolean isClasseA);
	
	public void operation () {
		isClassA				= !isClassA;
		AbstractClass aClass	= createClass (isClassA);
		aClass.displayClass();
	}
}
