package ppa.model.pattern.rules.specification;

public class NotSpecification<T, V> extends AbstractCompositeSpecification<T, V> {
	
	@SafeVarargs
	public NotSpecification(Specification<T, V>... specifications){
		super(specifications);
	}

	@Override
	public boolean isSatisfiedBy(final T candidate, final V datas) {
        return !specifications.get(0).isSatisfiedBy(candidate, datas);
	}
}
