package ppa.model.pattern.rules.specification;

public abstract class LeafSpecification<T, V> extends AbstractCompositeSpecification<T, V>{
	public abstract boolean isSatisfiedBy(T candidate, V datas);
}
