package ppa.model.pattern.rules.specification;

/**
 * What is the purpose of this pattern ?
 * Define functional rules,
 * Compose several functional rules,
 * Get the exact implementation of these functional rules as developped,
 * Quickly Activate / Deactivate a rule without restart application server.
 */

/**
 * 
 * @author PPAVIA
 *
 * @param <T>
 */
public interface Specification<T, V> {
    public boolean isSatisfiedBy(T candidate, V datas);

    public Specification<T, V> or(Specification<T, V> specification);
    public Specification<T, V> and(Specification<T, V> specification);
    public Specification<T, V> not();
}
