package ppa.model.pattern.rules.specification;

public class AndSpecification<T, V> extends AbstractCompositeSpecification<T, V> {
	
	@SafeVarargs
	public AndSpecification(Specification<T, V>... specifications){
		super(specifications);
	}

	@Override
	public boolean isSatisfiedBy(final T candidate, final V datas) {
		boolean result = true;

        for (Specification<T, V> specification : this.specifications) {
            result &= specification.isSatisfiedBy(candidate, datas);
        }
        return result;
	}

}
