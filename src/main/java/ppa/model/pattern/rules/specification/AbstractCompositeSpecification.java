package ppa.model.pattern.rules.specification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractCompositeSpecification<T, V> implements Specification<T, V> {
	protected List<Specification<T, V>> specifications;

	@SafeVarargs
	protected AbstractCompositeSpecification(Specification<T, V>... specifications) {
		this.specifications = new ArrayList<Specification<T, V>>(Arrays.asList(specifications));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Specification<T, V> and(Specification<T, V> other) {
		return new AndSpecification<T, V>(this, other);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Specification<T, V> or(Specification<T, V> other) {
		return new OrSpecification<T, V>(this, other);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Specification<T, V> not() {
		return new NotSpecification<T, V>(this);
	}
}
